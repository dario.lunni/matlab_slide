function [] = state_plot(state, conversion, target)
    
    fprintf('\nPlotting...\n')
    time = state.time;
    pos_dot_BLSD = state.pos_dot_BLSD;
    pos_dot_DC = state.pos_dot_DC;
    pos_BLSD = state.pos_BLSD;
    pos_DC = state.pos_DC;
    pos_dot_DC_targ = state.pos_dot_DC_targ;
    pos_DC_targ = state.pos_DC_targ;

    pos_dot_target_BLSD = target.pos_dot_BLSD;
    pos_dot_target_DC = target.pos_dot_DC;
    pos_target_BLSD = target.pos_BLSD;
    pos_target_DC = target.pos_DC;
    time_normalized = (time - time(1))/1000; % [ms->s]

%     figure(1), subplot(2,1,1)
%     plot(time_normalized, pos_dot_target_BLSD, 'linewidth', 2), hold on
%     plot(time_normalized, pos_dot_BLSD, 'linewidth', 2), hold on
    % subplot(2,1,2), plot(pos_dot_BLSD, 'linewidth', 1)
    %plot(t_micro_norm, pos_dot_avg_DC_test), hold on
    % plot(t_micro_norm, pos_dot_PC, '--','linewidth', 1)
%     legend('Target Speed','Measured Speed')
%     title('Position and Speed')
    %
%     figure(1), subplot(2,1,2)
    % plot(t, pos_target_BLSD/conversion*360, 'linewidth', 2), hold on
%     plot(time_normalized, pos_BLSD/conversion.BLSD*360,'linewidth', 2)
%     plot(t_micro_norm, pos_PC/conversion, '--','linewidth', 1)
%     legend('Target Position','Measured Position')
    %
%     figure(2), subplot(2,1,1)
    % plot(t, pos_dot_target_DC, 'linewidth', 2), hold on
%     plot(time_normalized, pos_dot_DC, 'linewidth', 2), hold on
%     plot(time_normalized, pos_dot_DC_targ, '--', 'linewidth', 2)

%     figure(2), subplot(2,1,2)
    % plot(t, pos_target_DC, 'linewidth', 2), hold on
%     plot(time_normalized, pos_DC/conversion.DC, 'linewidth', 2), hold on
%     plot(time_normalized, pos_DC_targ/conversion.DC, '--', 'linewidth', 2)
%     % title('Error')
%     size(time_normalized)
%     size(pos_dot_target_BLSD)

    figure(1)
    
    subplot(2,1,1)
    plot(time_normalized, pos_dot_target_BLSD(1:end-1), '--', 'linewidth', 2), hold on
    plot(time_normalized, pos_dot_BLSD, 'linewidth', 2), hold on
    legend('Target Position','Measured Speed')
    title('Target Speed and Position BLSD - Rotational Joint')
    
    subplot(2,1,2)
    plot(time_normalized, pos_target_BLSD(1:end-1)/conversion.BLSD*360, '--', 'linewidth', 2), hold on
    plot(time_normalized, pos_BLSD/conversion.BLSD*360,'linewidth', 2)
    legend('Target Position','Measured Position')
    
    figure(2)
    
    subplot(2,1,1), plot(time_normalized, pos_dot_target_DC(1:end-1), '--', 'linewidth', 2), hold on
    plot(time_normalized, pos_dot_DC, 'linewidth', 2), hold on
    legend('Target Position','Measured Speed')
    title('Target Speed and Position DC - Linear Joint')
    
    subplot(2,1,2), plot(time_normalized, pos_target_DC(1:end-1)/conversion.DC, '--', 'linewidth', 2), hold on
    plot(time_normalized, pos_DC/conversion.DC, 'linewidth', 2), hold on
    legend('Target Position','Measured Position')
   

end

