function [target] = trajectory_generation(A, freq, A_BLSD, freq_BLSD, t_total, plot_flag, conversion)
% Trajectory generation function
% The output of the function is a structure containing target speed and 
% position of the two robot joints:
% - the rotational joint is the BLSD motor
% - the linear joint is the DC motor
% Here is reported the general structure used to send messages to the
% MCU:
% target.pos_dot_BLSD = pos_dot_target_BLSD;
% target.pos_dot_DC = pos_dot_target_DC;
% target.pos_BLSD = pos_target_BLSD;
% target.pos_DC = pos_target_DC;
% target.t_sample = t_sample;
% target.count = count;
% target.t_total = t_total;
% Any other function can be used to generate an arbitrary trajectory 
% but keep in mind that:
% - the low level control is built up using encoder counts, so the appropriate
% conversion need to be applied if the gear box will be changed
% - the sending messages frequency is strongly limited by the used hardware,
% it has been tested at 0.05 and it SHOULD NOT be changed


t_sample = 0.05;            % sample time, DO NOT CHANGE
t = [0:t_sample:t_total];   % time array
count = length(t);          % total number of messages

pos_dot_target_BLSD = A_BLSD*sin(2*pi*t*freq_BLSD); % target speed array

% pos_dot_filtered = pos_dot; %lowpass(pos_dot, 2, 1/t_sample); % to use a
% low pass filter on the desired velocity.

% velocity integration
for ii = 2:count
    %     pos_filtered(ii) = trapz(t(1:ii)*1000000, pos_dot_filtered(1:ii));
    pos_target_BLSD(ii) = trapz(t(1:ii)*1000000, pos_dot_target_BLSD(1:ii));
end

pos_dot_target_DC = A*sin(2*pi*t*freq); % target speed array

%pos_dot_filtered_DC = pos_dot_DC; %lowpass(pos_dot, 2, 1/t_sample);% to use a
% low pass filter on the desired velocity.

% velocity integration
for ii = 2:count
    %     pos_filtered_DC(ii) = trapz(t(1:ii)*1000000, pos_dot_filtered(1:ii));
    pos_target_DC(ii) = trapz(t(1:ii)*1000000, pos_dot_target_DC(1:ii));
end

if plot_flag == true
    figure(1)

    subplot(2,1,1)
    plot(t, pos_dot_target_BLSD, '--', 'linewidth', 2), hold on
    legend('Target Position')
    title('Target Speed and Position BLSD - Rotational Joint')

    subplot(2,1,2)
    plot(t, pos_target_BLSD/conversion.BLSD*360, '--', 'linewidth', 2), hold on
    legend('Target Position')

    figure(2)

    subplot(2,1,1)
    plot(t, pos_dot_target_DC, '--', 'linewidth', 2), hold on
    legend('Target Position')
    title('Target Speed and Position DC - Linear Joint')

    subplot(2,1,2)
    plot(t, pos_target_DC/conversion.DC, '--', 'linewidth', 2), hold on
    legend('Target Position')
end

% Structure output containing Target Trajecories
target.pos_dot_BLSD = pos_dot_target_BLSD;
target.pos_dot_DC = pos_dot_target_DC;
target.pos_BLSD = pos_target_BLSD;
target.pos_DC = pos_target_DC;
target.t_sample = t_sample;
target.count = count;
target.t_total = t_total;

end

