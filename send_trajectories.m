function [state] = send_trajectories(target)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open serial communication
disp('Available serial port ... ')
disp(serialportlist())
SerialPort = 'COM6';
teensy = serialport(SerialPort, 115200);
fprintf('Communication open on Serial Port: %s \n', SerialPort);
fprintf('-----------------------\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up timer 
t_obj = timer;
msg.pos_dot_BLSD = target.pos_dot_BLSD;
msg.pos_dot_DC = target.pos_dot_DC;
msg.pos_BLSD = target.pos_BLSD;
msg.pos_DC = target.pos_DC;
msg.count = 0;
msg.serial = teensy;

set(t_obj, 'ExecutionMode', 'fixedRate');
set(t_obj, 'Period', target.t_sample);
set(t_obj, 'TasksToExecute', target.count);
set(t_obj, 'StartFcn', {@tx_serial});
set(t_obj, 'TimerFcn', {@tx_serial});
set(t_obj, 'StopFcn', {@tx_serial});
set(t_obj, 'UserData', msg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize received messages
global received_msg
received_msg = cell(target.count,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% take initial time and start to send messages at fixed frequency
tic
start(t_obj)

% wait to proceed
pause(target.t_total)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stop transmission
stop(t_obj)
delete(t_obj)
flush(teensy, "output");
clear teensy
fprintf('\nend...\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save output in state
state = post_proc(received_msg);

end

