function [state] = post_proc(received_msg)
% Post-processing received messages
for ii = 1:length(received_msg)-1
    c = split(received_msg{ii+1}(2:end-2));
    %     pos_dot_PC(ii) = str2num(c{1}); % data sent
    %     pos_PC(ii) = str2num(c{2});
    %     t_micro(ii) = str2num(c{3}); % Measured time
    %     P(ii) = str2num(c{4}); % Proportional component
    %     I(ii) = str2num(c{5}); % Integral component
    %     pos_dot_targ(ii) = str2num(c{6}); % Speed target
    %     pos_dot_avg(ii) = str2num(c{7}); % Measured Speed
    %     pos_targ(ii) = str2num(c{8}); % Position target
    %     pos_micro(ii) = str2num(c{9}); % Measures position
    %     pos_dot_PC_DC(ii) = str2num(c{10}); % Print delta time
    % msg_count(ii) = str2num(c{11}); % Number of received msg
    time(ii) = str2num(c{1}); % data sent
    pos_dot_BLSD(ii) = str2num(c{2}); % data sent
    pos_dot_DC(ii) = str2num(c{3}); % data sent
    pos_BLSD(ii) = str2num(c{4}); % data sent
    pos_DC(ii) = str2num(c{5}); % data sent
    pos_dot_DC_targ(ii) = str2num(c{6}); % Print delta time
    pos_DC_targ(ii) = str2num(c{7});%
end

state.time = time;
state.pos_dot_BLSD = pos_dot_BLSD;
state.pos_dot_DC = pos_dot_DC;
state.pos_BLSD = pos_BLSD;
state.pos_DC = pos_DC;
state.pos_dot_DC_targ = pos_dot_DC_targ;
state.pos_DC_targ = pos_DC_targ;
end

