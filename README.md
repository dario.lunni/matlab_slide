# MATLAB-MCU communication with Teensy-ESCON Carrier Board (Escon 24/2 Driver).

This repo contains the MATLAB script to communicate with _TEENSY-ESCON Carrier Board_.

## Installation

No additional software is required to run the script. The script has been tested on MATLAB2019b. 

## Usage

The `main.m` script calls the three main functions of the code:

1. [trajectory_generation](### trajectory_generation)
2. [send_trajectories](### send_trajectories)
3. [state_plot](### state_plot)

### trajectory_generation
This function generates the trajectoris for the two motors. At the actual stage this block only generates sinusoidal trajectories used as target speed starting from amplitude and frequency (desired pulse/us of the encoder are used as amplitude for the sinusoidal signal). The target speed is then integrated to obtain the correspondet target position.
The generated trajectories are characterized by a sampling frequency of 20 Hz (0.05 s). On this frequency the serial communication has been set up and the controller has been tuned. 

### send_trajectories
This function set up the communication and send each element of the trajecotry every (nearly) 0.05 s. The communication is set up through the parameter `SerialPort` in which the correct port should be inserted (default is `COM6`). A list of available port are printed just before the communication starting.
The message is automatically created and will be composed by:
1. Rotational speed
2. Rotational position
3. Linear speed
4. Linear position 

### state_plot
The received messages are saved in `state` structure and than passed to the plot function.

## Contributing
For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
