% Clear all variables
clear all, close all, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conversion factor to transform physical units in encoder pulses
conversion.BLSD = 512*4*14;     % counts/conversion = theta [degree]
conversion.DC = 1000*4*5.2/10;  % counts/conversion = x [mm]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Trajectory Generation
A = 0.065;          % [count/us]
freq = .05;         % [Hz]
A_BLSD = 0.01;      % [count/us]
freq_BLSD = 0.1;    % [Hz]
t_total = 1/freq;   % [us]
plot_flag = false;  % to plot during the trajectory generation phase
% WARN: do not plot before if the whole script is run

target_trajectories = trajectory_generation(A, freq, A_BLSD, freq_BLSD, t_total, plot_flag, conversion);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start communication with micro and save system state
state = send_trajectories(target_trajectories);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot target and measured trajectories
plot_flag = true;
state_plot(state, conversion, target_trajectories)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


