function [msg] = tx_serial(obj, event)
global received_msg
event_type = event.Type;

% get times executed
exec_count = get(obj, 'TasksExecuted');

% get msg
msg     = get(obj, 'UserData');

v       = getfield(msg, 'pos_dot_BLSD');
v_DC    = getfield(msg, 'pos_dot_DC');
p       = getfield(msg, 'pos_BLSD');
p_DC    = getfield(msg, 'pos_DC');
% received_msg = getfield(msg, 'received_msg');
ii      = getfield(msg, 'count');
board   = getfield(msg, 'serial');

switch event_type
    
    case 'StartFcn'
        fprintf('Start transmission... \n');
        fprintf('-----------------------\n');
        writeline(board, ['[' 'enable' ']']);
        fprintf('Transmitting...\n');
        
    case 'TimerFcn'
        ii = ii + 1;
        msg_str =  ['<' num2str(v(exec_count),5) ',' ...
            num2str(p(exec_count),8) ',' ...
            num2str(v_DC(exec_count),5) ',' ...
            num2str(p_DC(exec_count),8) '>'];
        str = writeread(board, msg_str);
        disp(msg_str);
        received_msg{ii} = str;
        % msg = setfield(msg, {1,1}, 'received_msg', {ii}, str);
        % disp(size(msg_str))
        sampled_time = toc;
        tic
        fprintf('Count: %2d \tTime: %.5f\n',...
            exec_count, ...
            sampled_time);
        
    case 'StopFcn'
        writeline(board, ['[' 'disable' ']']);
        fprintf('-----------------------');
        fprintf('\nEnd transmission...\n');
        fprintf('Data sent: %i', ii);
        
end

msg = setfield(msg, 'count', ii);
set(obj, 'UserData', msg);

end
